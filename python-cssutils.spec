%global _empty_manifest_terminate_build 0
Name:		python-cssutils
Version:	2.3.1
Release:	1
Summary:	A CSS Cascading Style Sheets library for Python
License:	LGPL-2.1-or-later
URL:		http://cthedot.de/cssutils/
Source0:	https://files.pythonhosted.org/packages/source/c/cssutils/cssutils-%{version}.tar.gz
BuildArch:	noarch


%description
A Python package to parse and build CSS Casecading Style Sheets.

%package -n python3-cssutils
Summary:	A CSS Cascading Style Sheets library for Python
Provides:	python-cssutils
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires: 	python3-pip  
BuildRequires: 	python-setuptools_scm  
BuildRequires: 	python3-toml 
%description -n python3-cssutils
python3 package for cssutils

%package help
Summary:	Development documents and examples for cssutils
Provides:	python3-cssutils-doc
%description help
help package for cssutils

%prep
%autosetup -n cssutils-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-cssutils -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_pkgdocdir}

%changelog
* Thu Nov 07 2024 panzhe <panzhe@kylinos.cn> - 2.3.1-1
- Update package to version 2.3.1
  * Enhance the techniques for file manipulation and processing

* Wed Apr 20 2022 jianli-97 <lijian2@kylinos.cn> - 2.3.0-2
- Delete the link of the License in the spec

* Fri Jul 23 2021 Xu Jin <jinxu@kylinos.cn> - 2.3.0-1
- Update package to 2.3.0
- Add Buildrequires:python3-pip python-setuptools_scm python3-toml

* Wed Jun 24 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated
